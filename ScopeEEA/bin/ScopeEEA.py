#!/opt/anaconda3/bin/python3
import sys
from PyQt5.QtCore import QObject, QThread, pyqtSignal,QTimer,QMutex,Qt
from PyQt5 import QtWidgets
import pyqtgraph as pg
import numpy as np
from datetime import datetime
import time



import pyvisa as visa
from pyumeea.instruments.visa.tektronix.scopes import *
from pyumeea.instruments.visa.agilent.scopes import *


class devicesClassHandler:
    knownDevList={}

    def __init__(self,visaInterface):
        self.className = 'appareil'
        self.visaInterface = visaInterface
        self.deviceList = self.find()
        self.currentDevice = None
        self.currentDeviceAddress = None
        self.currentDeviceShortIDN = None

    def openCurrentDevice(self,inAddr=None):
        if inAddr is None:
            if len(self.deviceList) == 0:
                raise ValueError('Aucun appareil de type '+self.className+' connecté ou reconnu !')
            #inAddr = list(self.deviceList.keys())[0]
        if inAddr not in  self.deviceList.keys():
            raise ValueError(self.className+' introuvable ou non reconnu !')
        driver = self.deviceList[inAddr][0]
        self.delCurrentDevice()
        self.currentDevice = driver(inAddr)
        self.currentDeviceAddress = inAddr
        self.currentDeviceShortIDN = self.deviceList[inAddr][1]

    def delCurrentDevice(self):
        if self.currentDevice != None:
            # Go To Local (does not seem to work)
            #app.scopes.currentScope.adapter.connection.control_ren(visa.constants.VI_GPIB_REN_ADDRESS_GTL)
            #print(dir(self.currentScope ))
            del self.currentDevice
        self.currentDevice = None
        self.currentDeviceAddress = None
        self.currentDeviceShortIDN = None


    def find(self):
        """Recherche de tous les appareils supportés sur les
        l'interface choisie"""
        rm = visa.ResourceManager(self.visaInterface)
        devList = rm.list_resources()

        driverList = []
        visaAddrList = []

        for dev in devList:
            if dev.upper().startswith('USB'):
                inst = rm.open_resource(dev)
                inst.write_termination='\n'
                instName = inst.query("*IDN?")
                for knownDevName,devDriver in self.knownDevList.items():
                    print(knownDevName)
                    if instName[0:len(knownDevName)].upper() == knownDevName.upper():
                        driverList.append((devDriver,knownDevName))
                        visaAddrList.append(dev)
                        inst.close()
                del inst
        rm.close()
        del rm
        if len(visaAddrList) == 0:
            raise ValueError('Aucun '+self.className+' connecté ou reconnu !')
        return dict(zip(visaAddrList,driverList))

    def resetParameters(self):
        self.currentDevice.adapter.write('*RST')
    def clearParameters(self):
        self.currentDevice.adapter.write('*CLS')

    def __del__(self):
        self.delCurrentDevice()




class scopesHandler(devicesClassHandler):
    """Classe pour gérer les oscilloscopes connectés sous forme d'une interface homogène"""
    knownDevList = {'TEKTRONIX,TDS 1012':TDS1012,
                'TEKTRONIX,TDS 1012A':TDS1012,
                'TEKTRONIX,TDS 1012B':TDS1012,
                'TEKTRONIX,TDS 1012C':TDS1012,
                'TEKTRONIX,TDS 1012C-EDU':TDS1012,
                'TEKTRONIX,TBS 1202B':TBS1202,
                'AGILENT TECHNOLOGIES,DSO-X 2002A':DSOX2002,
                'AGILENT TECHNOLOGIES,DSO1002A':DSO1002A,
                 'KEYSIGHT TECHNOLOGIES,EDU-X 1002':EDUX1002G
               }

    def __init__(self,visaInterface):
        super().__init__(visaInterface)
        self.className='oscilloscope'

    def prepareChannel(self,channel):
        # force AC mode and remove offset
        channelObj = self.getChannel(channel)
        channelObj.coupling = 'ac'
        channelObj.offset = 0
        channelObj.probeFactor='1X'
        channelObj.display='on'


    def startAndWaitAcquisition(self):
        self.currentDevice.single()
        self.currentDevice.trigger.waitStopStatus()

    def rangeTimeBase(self,totalTime):
        nbDivs = 10
        self.currentDevice.timebase.scale=totalTime/nbDivs

    def getChannel(self,channel):
        if channel=='channel1':
            channelObj = self.currentDevice.channel1
        elif channel=='channel2':
            channelObj = self.currentDevice.channel2
        else:
            raise ValueError('Voie inexistante !')
        return channelObj

    def changeScaleBy(self,channel,factor):
        channelObj = self.getChannel(channel)
        actValue= channelObj.scale
        channelObj.scale = actValue*factor

    def getVerticalLimits(self,channel,margin=1):
        nbDivs = 8
        channelObj = self.getChannel(channel)
        sensitivity = channelObj.scale
        offset = channelObj.offset

        return ((-sensitivity*nbDivs/2+offset)*margin, (sensitivity*nbDivs/2+offset)*margin)


    def getData(self,channel):
        """Acquisition avec un oscilloscope reconnu.
        La voie "channel" est channel1, channel2 ou math.
        Modèles reconnus : Tektronix TDS/TPS/TBS, Agilent DSO.
        Renvoie la trace sous forme de (x,y) en array numpy
        """
        x,y,*_=self.currentDevice.waveform.download(channel)
        y = (y + self.getChannel(channel).offset)
        return (x,y)

    def getDataToCSV(self,channel,filename):
        """Acquisition avec un oscilloscope reconnu.
        "driver" est le nom de l'objet pymeasure qui sert de driver
        "visaAddr" est l'adresse VISA de l'appareil
        La voie "channel" est channel1, channel2 ou math.
        Le fichier "filename" sera au format CSV.
        Modèles reconnus : Tektronix TDS/TPS/TBS, Agilent DSO.
        """
        import numpy as np

        x,y= self.getData(channel)
        toStore = np.column_stack((x,y))

        np.savetxt(filename, toStore,  delimiter=',',
                      header='X,Y_%s'%(channel))

    def checkMathMode(self):
        return self.currentDevice.math.operation.strip(' \n\r')


    def lockPanel(self,mode):
        if mode==True:
            self.currentDevice.lockPanel='on'
        else:
            self.currentDevice.lockPanel='off'


class GenericAcquisitionWorker(QObject):
    progress = pyqtSignal()

    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.mutex = QMutex()
        self.pleaseRunFlag = None

    def pleaseRun(self):
        if self.pleaseRunFlag == False:
            if self.mutex.tryLock():
                self.mutex.unlock()
            else:
                self.mutex.unlock()
            self.pleaseRunFlag = True

    def pleasePause(self):
        self.pleaseRunFlag = False

    def pleaseRunStatus(self):
        return self.pleaseRunFlag

    def run(self):
        while True:
            if self.mutex.tryLock():
                if self.pleaseRunFlag == True :
                    self.runningProcess()
                    self.progress.emit()
                    self.mutex.unlock()
            else:
                self.idlingProcess()

    def runningProcess(self): pass

    def idlingProcess(self):pass

class AcquisitionWorker(GenericAcquisitionWorker):
    def __init__(self,parent,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.parent = parent

    def runningProcess(self):
        self.parent.runProc()

    def idlingProcess(self):
        time.sleep(0.1)

def showThread(name):
    print("%s : %s Thread 0x%x"%(datetime.now().strftime("%H:%M:%S"),name,int(QThread.currentThreadId())))

class infoWindow(QtWidgets.QWidget):
    aboutToClose = pyqtSignal()

    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.setWindowTitle('Informations ...')
        self.setMinimumWidth(800)
        self.setMinimumHeight(400)

        self.GuiMainLayout={}
        self.GuiMainLayout['oscListText'] = QtWidgets.QTextEdit()
        #self.GuiMainLayout['coreText'].setTextFormat(Qt.RichText)
        html ='<p><b>Liste des oscilloscopes supportés par ce logiciel :</b><p/><p>'
        for name in app.scopes.knownDevList.keys():
            html+=name+'<br/>'
        html+='</p>'
        self.GuiMainLayout['oscListText'].setHtml(html)
        self.GuiMainLayout['oscListText'].setReadOnly(True)
        self.GuiMainLayout['pythonHelper'] = QtWidgets.QTextEdit()
        html = "<p><b>Pour exporter les traces de l'oscilloscope vers Python :</b><br/> 1. Exporter la trace en CSV avec ce logiciel. <br/>2. Ensuite, voici un exemple de code  qui permet d'importer le fichier CSV dans Python. On se place dans le cas où  vous avez sauvé uniquement les voies 1 et 2. Vous pouvez  copier/coller le code directement dans votre script et adapter :</p>"
        html+='<p><pre>'
        html+='import numpy as np\n'
        html+='import matplotlib.pyplot as plt\n'
        html+="nom_fichier_csv = 'my_file.csv'\n"
        html+= "my_data = np.genfromtxt(nom_fichier_csv, delimiter=',',skip_header=1)\n"
        html+= "t1=my_data[:,0];   v1=my_data[:,1];   t2=my_data[:,2];   v2=my_data[:,3];\n"
        html+= "plt.figure()\n"
        html+= "plt.plot(t1,v1,label='voie 1')\n"
        html+= "plt.plot(t2,v2,label='voie 2')\n"
        html+= "plt.legend()\n"
        html+='</pre></p>'
        html+='<p>Comme sur cet exemple on suppose que 2 voies seulement ont été sauvegardées, <code>my_data</code> ne comporte que 4 colonnes (2 couples temps,tension).</p>'

        self.GuiMainLayout['pythonHelper'].setHtml(html)
        self.GuiMainLayout['pythonHelper'].setReadOnly(True)
        self.GuiMainLayout['matlabHelper'] = QtWidgets.QTextEdit()
        html = "<p><b>Pour exporter les traves de l'oscilloscope vers Matlab/Octave :</b><br/> 1. Exporter la trace en CSV avec ce logiciel. <br/>2. Ensuite, voici un exemple de code  qui permet d'importer le fichier CSV dans Matlab/Octave. On se place dans le cas où  vous avez sauvé uniquement les voies 1 et 2. Vous pouvez  copier/coller le code directement dans votre script et adapter :</p>"
        html+='<p><pre>'
        html+="mon_fichier_csv = 'my_file.csv';\n"
        html+="formatSpec = '%f%f%f%f%[^\\n\\r]';\n"
        html+="fileID = fopen(mon_fichier_csv,'r');\n"
        html+="dataArray = textscan(fileID, formatSpec, 'Delimiter', ',', 'HeaderLines' ,1, 'ReturnOnError', false, 'EndOfLine', '\\r\\n');\n"
        html+="fclose(fileID);\n"
        html+="X_1 = dataArray{:, 1};  Y_1 = dataArray{:, 2};  X_2 = dataArray{:, 3};  Y_2 = dataArray{:, 4};\n"
        html+="plot(X_1,Y_1)\n"
        html+="hold on\n"
        html+="plot(X_2,Y_2)\n"
        html+="legend({'voie 1','voie 2'})\n"
        html+='</pre></p>'
        html+='<p>Comme sur cet exemple on suppose que 2 voies seulement ont été sauvegardées, <code>my_data</code> ne comporte que 4 colonnes (2 couples temps,tension).</p>'

        self.GuiMainLayout['matlabHelper'].setHtml(html)
        self.GuiMainLayout['matlabHelper'].setReadOnly(True)

        self.GuiMainLayout['coreTabs'] = QtWidgets.QTabWidget()
        self.GuiMainLayout['coreTabs'].addTab(self.GuiMainLayout['pythonHelper'], "Python")
        self.GuiMainLayout['coreTabs'].addTab(self.GuiMainLayout['matlabHelper'], "Matlab/Octave")
        self.GuiMainLayout['coreTabs'].addTab(self.GuiMainLayout['oscListText'], "Liste des Oscilloscopes")

        self.GuiMainLayout['mainL'] = QtWidgets.QGridLayout()


        self.GuiMainLayout['mainL'].addWidget(self.GuiMainLayout['coreTabs'],0,0)
        self.setLayout(self.GuiMainLayout['mainL'])

    def closeEvent(self,event):
        super().closeEvent(event)
        self.aboutToClose.emit()


class Window(QtWidgets.QMainWindow):

    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.buildGUILayout()
        #self.buildPausedAcquisitionWorker()
        #self.buildTimer()
        self.x = np.linspace(0,10,1000)
        self.y = np.linspace(0,10,1000)
        self.penCh1 = {'color':'r'}#,'width': 2}
        self.penCh2 = {'color':'b'}#,'width': 2}
        self.penMath = {'color':(128, 0, 255)}#,'width': 2}
        self.brushLegend = pg.mkBrush("FFFFFFCC")
        self.penLegend = {'color':'k'}#,'width': 1}
        self.infoWindow = None
        self.acquisitionMutex = QMutex()


    def workerProgress(self):pass
        #self.GuiMainLayout['pbar'].setValue(random.randint(0, 100))
        #showThread('Report')
        #print("\n")

    def mainAppProgress(self):
        #showThread('Timer')
        if(self.acquisitionWorker.pleaseRunStatus()):
            self.GuiMainLayout['timePlot'].plot(x=self.x,y=self.y,  clear=True, _callSync='off',pen=self.thePen)

    def runProc(self):
        sleep(0.1)
        #self.x = np.linspace(0,10,10000)
        #self.y = np.random.rand(*self.x.shape)
        #time.sleep(0.1)
        #showThread('Acquisition Worker')

    def buildPausedAcquisitionWorker(self):
        self.thread = QThread()
        self.acquisitionWorker = AcquisitionWorker(self)
        self.acquisitionWorker.moveToThread(self.thread)
        self.thread.started.connect(self.acquisitionWorker.run)
        self.acquisitionWorker.progress.connect(self.workerProgress)
        self.acquisitionWorker.pleasePause()
        self.thread.start()

    def buildTimer(self):
        self.timer = QTimer()
        self.timer.setInterval(100)
        self.timer.timeout.connect(self.mainAppProgress)
        self.timer.start()

    def buildGUILayout(self):
        self.setWindowTitle('Acquisition de traces sur osciloscope')

        self.GuiMainLayout={}
        # choose scope part
        self.GuiMainLayout['scopesSupported'] = QtWidgets.QPushButton("Liste des Oscilloscopes\nSupportés")
        self.GuiMainLayout['scopesSupported'].clicked.connect(self.signal_scopesSupported)
        self.GuiMainLayout['pythonImport'] = QtWidgets.QPushButton("Exporter\nvers Python")
        self.GuiMainLayout['pythonImport'].clicked.connect(self.signal_pythonExample)
        self.GuiMainLayout['matlabImport'] = QtWidgets.QPushButton("Exporter\nvers Matlab")
        self.GuiMainLayout['matlabImport'].clicked.connect(self.signal_matlabExample)
        self.GuiMainLayout['scopeGroup'] = QtWidgets.QGroupBox("Choix d'un Oscilloscope")
        self.GuiMainLayout['scopeCombo'] = QtWidgets.QComboBox()
        self.GuiMainLayout['scopeCombo'].setMinimumWidth(100)
        self.GuiMainLayout['scopeCombo'].setMaximumWidth(300)
        for visaAddr,visaDev in app.scopes.deviceList.items():
            name = visaDev[1]
            addr = visaAddr
            self.GuiMainLayout['scopeCombo'].addItem(name+' [%s]'%addr)
        self.GuiMainLayout['scopeCombo'].activated.connect(self.signal_scopeChanged)
        self.signal_scopeChanged(0)

        ## Build Layout for scope choice
        self.GuiMainLayout['scopeGroupLayout'] = QtWidgets.QHBoxLayout()
        self.GuiMainLayout['scopeGroupLayout'].addWidget(self.GuiMainLayout['scopeCombo'])
        spacer = QtWidgets.QSpacerItem(10,10,QtWidgets.QSizePolicy.Expanding,QtWidgets.QSizePolicy.Minimum)
        self.GuiMainLayout['scopeGroupLayout'].addSpacerItem(spacer)
        self.GuiMainLayout['scopeGroupLayout'].addWidget(self.GuiMainLayout['pythonImport'])
        self.GuiMainLayout['scopeGroupLayout'].addWidget(self.GuiMainLayout['matlabImport'])
        self.GuiMainLayout['scopeGroupLayout'].addWidget(self.GuiMainLayout['scopesSupported'])
        self.GuiMainLayout['scopeGroup'].setLayout(self.GuiMainLayout['scopeGroupLayout'])


        # plots display part
        self.GuiMainLayout['displayGroup']=QtWidgets.QGroupBox("Traces")
        #self.GuiMainLayout['label'] = QtWidgets.QLabel('test')
        pg.setConfigOption('antialias',True)
        pg.setConfigOption('background', 'w')
        pg.setConfigOption('foreground', 'k')
        #pg.setConfigOption('useOpenGL',True)
        pg.setConfigOption('leftButtonPan',False)
        self.GuiMainLayout['timePlot'] = pg.PlotWidget()
        self.GuiMainLayout['fourierPlot'] = pg.PlotWidget()
        self.GuiMainLayout['fourierPlot'].hide()
        ## Build Layout for display part
        self.GuiMainLayout['displayGroupLayout']=QtWidgets.QVBoxLayout()
        #self.GuiMainLayout['displayGroupLayout'].addWidget(self.GuiMainLayout['label'])
        self.GuiMainLayout['displayGroupLayout'].addWidget(self.GuiMainLayout['timePlot'],1)
        self.GuiMainLayout['displayGroupLayout'].addWidget(self.GuiMainLayout['fourierPlot'],1)
        self.GuiMainLayout['displayGroup'].setLayout(self.GuiMainLayout['displayGroupLayout'])

        # tools part
        self.GuiMainLayout['toolsGroup']=QtWidgets.QGroupBox("Pilotage")
        self.GuiMainLayout['channel_1'] = QtWidgets.QCheckBox('Voie 1')
        self.GuiMainLayout['channel_2'] = QtWidgets.QCheckBox('Voie 2')
        self.GuiMainLayout['channel_math'] = QtWidgets.QCheckBox('Math')
        self.GuiMainLayout['channel_1'].setChecked(True)
        self.GuiMainLayout['channel_2'].setChecked(True)
        self.GuiMainLayout['channel_math'].setChecked(False)
        self.GuiMainLayout['download'] = QtWidgets.QPushButton("Récupération\ndes signaux")
        self.GuiMainLayout['download'].clicked.connect(self.signal_downloadData)
        self.GuiMainLayout['export'] = QtWidgets.QPushButton("Exportation\nen CSV")
        self.GuiMainLayout['export'].clicked.connect(self.signal_exportCSV)
        #####  Not activated for now
        self.GuiMainLayout['start'] = QtWidgets.QPushButton("Start")
        self.GuiMainLayout['start'].clicked.connect(self.signal_start)
        self.GuiMainLayout['stop'] = QtWidgets.QPushButton("Stop")
        self.GuiMainLayout['stop'].clicked.connect(self.signal_stop)

        self.GuiMainLayout['quit'] = QtWidgets.QPushButton("Quitter")
        self.GuiMainLayout['quit'].clicked.connect(app.quit)
        ## Build Layout for tools part
        self.GuiMainLayout['toolsGroupLayout']=QtWidgets.QVBoxLayout()
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['channel_1'])
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['channel_2'])
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['channel_math'])
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['download'])
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['export'])

        spacer = QtWidgets.QSpacerItem(10,10,QtWidgets.QSizePolicy.Minimum,QtWidgets.QSizePolicy.Expanding)
        self.GuiMainLayout['toolsGroupLayout'].addSpacerItem(spacer)
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['quit'])
        self.GuiMainLayout['toolsGroup'].setLayout(self.GuiMainLayout['toolsGroupLayout'])

        # Other elements
        self.GuiMainLayout['pbar'] = QtWidgets.QProgressBar()
        self.GuiMainLayout['pbar'].setGeometry(30, 40, 200, 25)
        self.GuiMainLayout['footer'] = QtWidgets.QLabel()
        self.GuiMainLayout['footer'].setTextFormat(Qt.RichText)
        from pyumeea.institution import base as eeahtml
        text = '<b>Acquisition de traces sur Oscilloscope</b> v. 1.1 (Apr.2021)'
        text += '<br/><i>Ecrit en Python 3.7/pyQt5 par <A HREF="mailto:mikhael.myara@umontpellier.fr">mikhael.myara@umontpellier.fr</A></i><br/> <Université de Montpellier'
        text += '<br/><i>Département EEA - Faculté des Sciences</i><br/></i>Université de Montpellier</i>'
        text=eeahtml.eeaWidgetHTML_Qt(credits=text)
        self.GuiMainLayout['footer'].setText(text)

        # the main layout
        self.GuiMainLayout['mainL'] = QtWidgets.QGridLayout()
        self.GuiMainLayout['mainL'].addWidget(self.GuiMainLayout['scopeGroup'],0,0,1,3)
        self.GuiMainLayout['mainL'].addWidget(self.GuiMainLayout['displayGroup'],1,0)
        self.GuiMainLayout['mainL'].addWidget(self.GuiMainLayout['toolsGroup'],1,1)
        self.GuiMainLayout['mainL'].addWidget(self.GuiMainLayout['pbar'],2,0,1,3)
        self.GuiMainLayout['mainL'].addWidget(self.GuiMainLayout['footer'],3,0,1,3)
        self.GuiMainLayout['main'] = QtWidgets.QWidget()
        self.GuiMainLayout['main'].setLayout(self.GuiMainLayout['mainL'])
        # attribute the layout to this window
        self.setCentralWidget(self.GuiMainLayout['main'])


    def signal_downloadData(self):
        if self.acquisitionMutex.tryLock():
            try:            
                app.scopes.lockPanel(True)         
                #print("YOUHOU")
                #print(app.scopes.currentDevice.ask(":WAVeform:YORigin?"))  
                #print(app.scopes.currentDevice.channel1.offset)     
                self.GuiMainLayout['fourierPlot'].hide()
                self.GuiMainLayout['timePlot'].addLegend(brush=self.brushLegend,pen=self.penLegend)
                self.GuiMainLayout['timePlot'].showGrid(x=True,y=True)
                self.GuiMainLayout['timePlot'].clear()
                if self.GuiMainLayout['channel_1'].checkState()==2:
                    app.ch1Data = app.scopes.getData('channel1')
                    self.GuiMainLayout['timePlot'].plot(x=app.ch1Data[0],y=app.ch1Data[1] ,pen=self.penCh1,name='Voie 1')
                else:
                    app.ch1Data = None;

                if self.GuiMainLayout['channel_2'].checkState()==2:
                    app.ch2Data = app.scopes.getData('channel2')
                    self.GuiMainLayout['timePlot'].plot(x=app.ch2Data[0],y=app.ch2Data[1] ,pen=self.penCh2,name='Voie 2')
                else:
                    app.ch2Data = None;

                if self.GuiMainLayout['channel_math'].checkState()==2:
                    app.mathData = app.scopes.getData('math')
                    if app.scopes.checkMathMode()=='FFT':
                        self.GuiMainLayout['fourierPlot'].show()
                        self.GuiMainLayout['fourierPlot'].clear()
                        self.GuiMainLayout['fourierPlot'].addLegend(brush=self.brushLegend,pen=self.penLegend)
                        self.GuiMainLayout['fourierPlot'].showGrid(x=True,y=True)
                        self.GuiMainLayout['fourierPlot'].plot(x=app.mathData[0],y=app.mathData[1] ,pen=self.penMath,name='Math')
                    else:
                        self.GuiMainLayout['timePlot'].plot(x=app.mathData[0],y=app.mathData[1] ,pen=self.penMath,name='Math')
                else:
                    app.mathData = None;
            except: 
                time.sleep(2000)
            
            app.scopes.lockPanel(False)
            self.acquisitionMutex.unlock()

    def signal_exportCSV(self):
        options = QtWidgets.QFileDialog.Options()
        #options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QtWidgets.QFileDialog.getSaveFileName(self,"QFileDialog.getSaveFileName()","","CSV Files (*.csv);;TSV Files (*.tsv);;Text Files (*.txt);;All Files (*)", options=options)

        if fileName != '':
            dataToStore=[]
            headers=[]
            if app.ch1Data != None :
                dataToStore.append(app.ch1Data[0])
                dataToStore.append(app.ch1Data[1])
                headers.append('X_1,Y_1')
            if app.ch2Data != None :
                dataToStore.append(app.ch2Data[0])
                dataToStore.append(app.ch2Data[1])
                headers.append('X_2,Y_2')
            if app.mathData != None :
                dataToStore.append(app.mathData[0])
                dataToStore.append(app.mathData[1])
                headers.append('X_M,Y_M')
            maxLen = -1
            for data in dataToStore:
                if maxLen < len(data):
                    maxLen = len(data)

            for k,data in enumerate(dataToStore):
                if maxLen-len(data)>0:
                    data2 = np.zeros(maxLen-len(data))+np.nan
                    newData = np.concatenate((data,data2))
                    dataToStore[k]=newData


            toStore = np.column_stack(dataToStore)
            header = ','.join(headers)

            np.savetxt(fileName, toStore,  delimiter=',',header=header)

    def signal_start(self):
        self.acquisitionWorker.pleaseRun()

    def signal_stop(self):
        self.acquisitionWorker.pleasePause()

    def signal_scopeChanged(self,forceIdx = None):
        if forceIdx == None:
            idx  =  self.GuiMainLayout['scopeCombo'].currentIndex()
        else:
            idx = forceIdx
        if(idx >= 0):
            scopeAddr = list(app.scopes.deviceList.keys())[idx]
            app.scopes.openCurrentDevice(scopeAddr)

    def createInfoWindow(self):
        if( self.infoWindow == None):
            theWindow = infoWindow()
            theWindow.setAttribute(Qt.WA_DeleteOnClose);
            theWindow.aboutToClose.connect(self.signal_infoWindowClosed)
            self.infoWindow = theWindow


    def signal_scopesSupported(self):
        self.createInfoWindow()
        self.infoWindow.GuiMainLayout['coreTabs'].setCurrentIndex(2)
        self.infoWindow.show()


    def signal_pythonExample(self):
        self.createInfoWindow()
        self.infoWindow.GuiMainLayout['coreTabs'].setCurrentIndex(0)
        self.infoWindow.show()


    def signal_matlabExample(self):
        self.createInfoWindow()
        self.infoWindow.GuiMainLayout['coreTabs'].setCurrentIndex(1)
        self.infoWindow.show()


    def closeEvent(self,event):
        if( self.infoWindow != None):
            self.infoWindow.close()
        super().closeEvent(event)

    def signal_infoWindowClosed(self):
        self.infoWindow = None

app = QtWidgets.QApplication(sys.argv)
visaDriver = ''
try:
    rm = visa.ResourceManager(visaDriver)
    rm.close()
    del rm
except:
    visaDriver='@py'


try:
    app.scopes = scopesHandler(visaDriver)
    app.mathData = None
    app.ch2Data = None
    app.ch1Data = None

    win = Window()
    win.show()
    sys.exit(app.exec())

    # error before GUI starts management
except ValueError as e:
    msgBox = QtWidgets.QMessageBox();
    msgBox.setText(str(e));
    msgBox.exec();

#except:
#    msgBox = QtWidgets.QMessageBox();
#    msgBox.setText("Erreur inconnue. Un appareil est peut-être utilisé dans une autre application en ce moment.")
#    msgBox.exec()

import sys
from PyQt5.QtCore import QObject, QThread, pyqtSignal,QTimer,QMutex,Qt
from PyQt5 import QtWidgets
from PyQt5 import QtGui

from scipy.optimize import differential_evolution
import pyqtgraph as pg
import numpy as np
from datetime import datetime
import time



import pyvisa as visa
from pyumeea.instruments.visa.tektronix.scopes import *
from pyumeea.instruments.visa.agilent.scopes import *
from  pyumeea.instruments.visa.agilent.fawg import *

class devicesClassHandler:
    knownDevList={}

    def __init__(self,visaInterface):
        self.className = 'appareil'
        self.visaInterface = visaInterface
        self.deviceList = self.find()
        self.currentDevice = None
        self.currentDeviceAddress = None
        self.currentDeviceShortIDN = None

    def openCurrentDevice(self,inAddr=None):
        if inAddr is None:
            if len(self.deviceList) == 0:
                raise ValueError('Aucun appareil de type '+self.className+' connecté ou reconnu !')
            #inAddr = list(self.deviceList.keys())[0]
        if inAddr not in  self.deviceList.keys():
            raise ValueError(self.className+' introuvable ou non reconnu !')
        driver = self.deviceList[inAddr][0]
        self.delCurrentDevice()
        self.currentDevice = driver(inAddr)
        self.currentDeviceAddress = inAddr
        self.currentDeviceShortIDN = self.deviceList[inAddr][1]

    def delCurrentDevice(self):
        if self.currentDevice != None:
            # Go To Local (does not seem to work)
            #app.scopes.currentScope.adapter.connection.control_ren(visa.constants.VI_GPIB_REN_ADDRESS_GTL)
            #print(dir(self.currentScope ))
            del self.currentDevice
        self.currentDevice = None
        self.currentDeviceAddress = None
        self.currentDeviceShortIDN = None


    def find(self):
        """Recherche de tous les appareils supportés sur les
        l'interface choisie"""
        rm = visa.ResourceManager(self.visaInterface)
        devList = rm.list_resources()

        driverList = []
        visaAddrList = []

        for dev in devList:
            if dev.upper().startswith('USB'):
                inst = rm.open_resource(dev)
                inst.write_termination='\n'
                instName = inst.query("*IDN?")
                for knownDevName,devDriver in self.knownDevList.items():
                    if instName[0:len(knownDevName)].upper() == knownDevName.upper():
                        driverList.append((devDriver,knownDevName))
                        visaAddrList.append(dev)
                        inst.close()
                del inst
        rm.close()
        del rm
        if len(visaAddrList) == 0:
            raise ValueError('Aucun '+self.className+' connecté ou reconnu !')
        return dict(zip(visaAddrList,driverList))

    def resetParameters(self):
        self.currentDevice.adapter.write('*RST')
    def clearParameters(self):
        self.currentDevice.adapter.write('*CLS')

    def __del__(self):
        self.delCurrentDevice()

class functionGeneratorsHandler(devicesClassHandler):

    """Classe pour gérer les appareils d'une classe donnée
    (oscilloscope, gbf, etc) connectés sous forme d'une interface
    Objet qui est pensé pour être hérité (voir scopesHandler par exemple)"""
    knownDevList = {'Agilent Technologies,33210A':Agilent33210A,
                    'Agilent Technologies,33220A':Agilent33220A,
                    'Agilent Technologies,33250A':Agilent33250A,
    }
    def __init__(self,visaInterface):
        super().__init__(visaInterface)
        self.className='generateur de fonctions'

    """ inShape should be one of the following strings : SINUSIOD, SQUARE, RAMP, PULSE, NOISE """
    def setFunction(self,inShape):
        # as long as we use only agilent, this function is so dummy
        self.currentDevice.shape=inShape

    def setFrequency(self,inFrequency):
        self.currentDevice.frequency=inFrequency

    def setAmplitude(self,inAmplitude):
        self.currentDevice.amplitude=inAmplitude

    def setOffset(self,inOffset):
        self.currentDevice.offset=inOffset

    def setOutput(self,inOnOff):
        self.currentDevice.output = inOnOff

    def lockPanel(self,mode):
        if mode==True:
            self.currentDevice.remote_local_state='REMOTE'
        else:
            self.currentDevice.remote_local_state='LOCAL'



class scopesHandler(devicesClassHandler):
    """Classe pour gérer les oscilloscopes connectés sous forme d'une interface homogène"""
    knownDevList = {#'TEKTRONIX,TDS 1012':TDS1012,
                #'TEKTRONIX,TDS 1012A':TDS1012,
                #'TEKTRONIX,TDS 1012B':TDS1012,
                #'TEKTRONIX,TDS 1012C':TDS1012,
                #'TEKTRONIX,TDS 1012C-EDU':TDS1012,
                #'TEKTRONIX,TBS 1202B':TBS1202,
                'AGILENT TECHNOLOGIES,DSO-X 2002A':DSOX2002,
                'AGILENT TECHNOLOGIES,DSO1002A':DSO1002A,
                'KEYSIGHT TECHNOLOGIES,EDU-X 1002':EDUX1002G
               }

    def __init__(self,visaInterface):
        super().__init__(visaInterface)
        self.className='oscilloscope'
        
    def prepareChannel(self,channel):
        # force AC mode and remove offset
        channelObj = self.getChannel(channel)
        channelObj.coupling = 'ac'
        channelObj.offset = 0
        channelObj.probeFactor='1X'
        channelObj.display='on'




    def startAndWaitAcquisition(self):
        import time           
        self.currentDevice.single()
        time.sleep(self.currentDevice.timebase.scale*10+0.01)


        if isinstance(self.currentDevice,DSOX2002) or isinstance(self.currentDevice,EDUX1002G):    
            trigStat = self.currentDevice.trigger.status().strip('\n\r')
            if trigStat!='STOP':
                self.currentDevice.write(":TRIGger:FORCe")

        self.currentDevice.trigger.waitStopStatus()

    def rangeTimeBase(self,totalTime):
        nbDivs = 10
        self.currentDevice.timebase.scale=totalTime/nbDivs

    def getChannel(self,channel):
        if channel=='channel1':
            channelObj = self.currentDevice.channel1
        elif channel=='channel2':
            channelObj = self.currentDevice.channel2
        else:
            raise ValueError('Voie inexistante !')
        return channelObj

    def changeScaleBy(self,channel,factor):
        channelObj = self.getChannel(channel)
        actValue= channelObj.scale
        if factor > 1 and actValue >=10*0.95 :
            raise ValueError('max atteint')
        elif factor < 1 and actValue <= 2e-3*1.05:
            raise ValueError('min atteint')
        else:
            channelObj.scale = actValue*factor

    def getVerticalLimits(self,channel,margin=1):
        nbDivs = 8
        channelObj = self.getChannel(channel)
        sensitivity = channelObj.scale
        offset = channelObj.offset

        return ((-sensitivity*nbDivs/2+offset)*margin, (sensitivity*nbDivs/2+offset)*margin)


    def getData(self,channel):
        """Acquisition avec un oscilloscope reconnu.
        La voie "channel" est channel1, channel2 ou math.
        Modèles reconnus : Tektronix TDS/TPS/TBS, Agilent DSO.
        Renvoie la trace sous forme de (x,y) en array numpy
        """
        x,y,*_=self.currentDevice.waveform.download(channel)
        y = (y + self.getChannel(channel).offset)
        return (x,y)

    def getDataToCSV(self,channel,filename):
        """Acquisition avec un oscilloscope reconnu.
        "driver" est le nom de l'objet pymeasure qui sert de driver
        "visaAddr" est l'adresse VISA de l'appareil
        La voie "channel" est channel1, channel2 ou math.
        Le fichier "filename" sera au format CSV.
        Modèles reconnus : Tektronix TDS/TPS/TBS, Agilent DSO.
        """
        import numpy as np

        x,y= self.getData(channel)
        toStore = np.column_stack((x,y))

        np.savetxt(filename, toStore,  delimiter=',',
                      header='X,Y_%s'%(channel))

    def checkMathMode(self):
        return self.currentDevice.math.operation.strip(' \n\r')

    def lockPanel(self,mode):
        if mode==True:
            self.currentDevice.lockPanel='on'
        else:
            self.currentDevice.lockPanel='off'


class BodeWorker(QObject):
    progress = pyqtSignal(object)
    log = pyqtSignal(str)

    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.mutex = QMutex()

    def importConditions(self,frequencies,inputVoltage):
        self.frequencies = frequencies
        self.inputVoltage = inputVoltage

    def run(self):
        self.abort = False
        #try:
        if True:
            frequencies = self.frequencies
            inputVoltage = self.inputVoltage

            app.functionGenerators.resetParameters()
            app.functionGenerators.clearParameters()
            app.scopes.resetParameters()
            app.scopes.clearParameters()
            app.scopes.prepareChannel('channel1')
            app.scopes.prepareChannel('channel2')
            app.functionGenerators.setOutput(False)
            app.functionGenerators.setFunction('SINUSOID')
            app.functionGenerators.setAmplitude(inputVoltage)
            app.functionGenerators.setFrequency(frequencies[0])
            app.functionGenerators.setOutput(True)
            for (k,freq) in enumerate(frequencies):
                if self.abort == True:
                    break
                # change input frequency
                self.log.emit("Changement de fréquence ..."+str(freq))
                app.functionGenerators.setFrequency(freq)
                app.scopes.rangeTimeBase(5/freq) # want 5 periods on screen

                # rescale if required
                rescalingFactor = 2.5
                allowRescalingCh1 = 11 # allow 10 changes, which is the max : 2.5^10 corresponds to 20V/2e-3 Vwhich is the max voltage variation
                allowRescalingCh2 = 11
                dataCh1=(np.empty(0),np.empty(0))
                dataCh2=(np.empty(0),np.empty(0))

                while allowRescalingCh1 >= 1 or allowRescalingCh2 >= 1:
                    app.scopes.startAndWaitAcquisition()
                    [dataCh1,allowRescalingCh1] = self.rescaleIfRequired('channel1',allowRescalingCh1,rescalingFactor)
                    [dataCh2,allowRescalingCh2] = self.rescaleIfRequired('channel2',allowRescalingCh2,rescalingFactor)
                    if self.abort == True:
                        break
                if self.abort == True:
                    break

                # here we are supposed to have stable input and output sine waves, we want to fit them
                # by means of genetic/differential evolution algorithm
                self.limit = True
                if self.limit == True:
                    if len(dataCh1[0])>=1000:
                        newTime=np.linspace(min(dataCh1[0]),max(dataCh1[0]),1000)
                        dataCh1_1=np.interp(newTime,dataCh1[0],dataCh1[1])
                        dataCh2_1=np.interp(newTime,dataCh2[0],dataCh2[1])
                        dataCh1=(newTime,dataCh1_1)
                        dataCh2=(newTime,dataCh2_1)

                fitCh1 = self.fitSin(dataCh1,freq)
                fitCh2 = self.fitSin(dataCh2,freq)
                module = fitCh2.x[0] / fitCh1.x[0]
                phase  = fitCh2.x[1] - fitCh1.x[1]
                phase = phase%(2*np.pi)
                if phase > np.pi :phase-=2*np.pi
                error  =  (np.sqrt(fitCh1.fun)+np.sqrt(fitCh2.fun))*50

                self.progress.emit({'step':k,'frequency':freq,'module':module,'phase':phase,'error':error})

            app.functionGenerators.setOutput(False)
            self.log.emit('####TERMINE####')
        #except ValueError as e:
        #    self.log.emit('####ERROR#### '+str(e))
        #except:
        #    self.log.emit('####ERROR#### '+str(sys.exc_info()[0]))


    def sineFunc(self,X):
        return X[0]*np.sin(2*np.pi*self.de_freq*self.de_data[0]+X[1])

    def sineErrFunc(self,X):
        ysin = self.sineFunc(X)
        return np.sum((ysin-self.de_data[1])**2)/np.sum(self.de_data[1]**2)

    def fitSin(self,data,freq):
        self.de_data  = data
        self.de_freq = freq
        magGuess = (max(data[1])-min(data[1]))/2
        bounds=[(magGuess/40,magGuess*1.5),(-np.pi,np.pi)]
        result  = differential_evolution(self.sineErrFunc, bounds,polish=False,popsize=40,tol=0.005)
        return result
    def rescaleIfRequired(self,channel,allowRescaling,rescalingFactor):
        data=app.scopes.getData(channel)
        [minVoltage,maxVoltage]=app.scopes.getVerticalLimits(channel,margin=0.95)
        try:
            if np.min(data[1])<minVoltage or np.max(data[1])>maxVoltage:
                app.scopes.changeScaleBy(channel,rescalingFactor)
                self.log.emit("Changement de calibre ..."+channel)
                allowRescaling -=1
            elif (np.min(data[1])*rescalingFactor) > minVoltage and (np.max(data[1])*rescalingFactor) < maxVoltage:
                app.scopes.changeScaleBy(channel,1/rescalingFactor)
                self.log.emit("Changement de calibre ..."+channel)
                allowRescaling -=1
            else:
                allowRescaling = 0
        except ValueError:
            allowRescaling = 0
        return data,allowRescaling


class infoWindow(QtWidgets.QWidget):
    aboutToClose = pyqtSignal()

    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.setWindowTitle('Informations ...')
        self.setMinimumWidth(800)
        self.setMinimumHeight(400)

        self.GuiMainLayout={}
        self.GuiMainLayout['devListText'] = QtWidgets.QTextEdit()
        #self.GuiMainLayout['coreText'].setTextFormat(Qt.RichText)
        html ='<p><b>Liste des oscilloscopes supportés par ce logiciel :</b><p/><p>'
        for name in app.scopes.knownDevList.keys():
            html+=name+'<br/>'
        html+='</p></br>'
        html+='<p><b>Liste des générateurs de fonction supportés par ce logiciel :</b><p/><p>'
        for name in app.functionGenerators.knownDevList.keys():
            html+=name+'<br/>'
        html+='</p>'
        self.GuiMainLayout['devListText'].setHtml(html)
        self.GuiMainLayout['devListText'].setReadOnly(True)

        #self.GuiMainLayout['coreText'].setTextFormat(Qt.RichText)



        self.GuiMainLayout['pythonHelper'] = QtWidgets.QTextEdit()
        html = "<p><b>Pour exporter les diagrammes de Bode vers Python :</b><br/> 1. Exporter la trace en CSV avec ce logiciel. <br/>2. Ensuite, voici un exemple de code  qui permet d'importer le fichier CSV dans Python. On se place dans le cas où  vous avez sauvé uniquement les voies 1 et 2. Vous pouvez  copier/coller le code directement dans votre script et adapter :</p>"
        html+='<p><pre>'
        html+='import numpy as np\n'
        html+='import matplotlib.pyplot as plt\n'
        html+="nom_fichier_csv = 'my_file.csv'\n"
        html+= "my_data = np.genfromtxt(nom_fichier_csv, delimiter=',',skip_header=1)\n"
        html+= "t1=my_data[:,0];   v1=my_data[:,1];   t2=my_data[:,2];   v2=my_data[:,3];\n"
        html+= "plt.figure()\n"
        html+= "plt.plot(t1,v1,label='voie 1')\n"
        html+= "plt.plot(t2,v2,label='voie 2')\n"
        html+= "plt.legend()\n"
        html+='</pre></p>'
        html+='<p>Comme sur cet exemple on suppose que 2 voies seulement ont été sauvegardées, <code>my_data</code> ne comporte que 4 colonnes (2 couples temps,tension).</p>'
        html+= "<p><b>Pour exporter les diagrammes de Bode vers Matlab/Octave :</b><br/> 1. Exporter la trace en CSV avec ce logiciel. <br/>2. Ensuite, voici un exemple de code  qui permet d'importer le fichier CSV dans Matlab/Octave. On se place dans le cas où  vous avez sauvé uniquement les voies 1 et 2. Vous pouvez  copier/coller le code directement dans votre script et adapter :</p>"
        html+='<p><pre>'
        html+="mon_fichier_csv = 'my_file.csv';\n"
        html+="formatSpec = '%f%f%f%f%[^\\n\\r]';\n"
        html+="fileID = fopen(mon_fichier_csv,'r');\n"
        html+="dataArray = textscan(fileID, formatSpec, 'Delimiter', ',', 'HeaderLines' ,1, 'ReturnOnError', false, 'EndOfLine', '\\r\\n');\n"
        html+="fclose(fileID);\n"
        html+="X_1 = dataArray{:, 1};  Y_1 = dataArray{:, 2};  X_2 = dataArray{:, 3};  Y_2 = dataArray{:, 4};\n"
        html+="plot(X_1,Y_1)\n"
        html+="hold on\n"
        html+="plot(X_2,Y_2)\n"
        html+="legend({'voie 1','voie 2'})\n"
        html+='</pre></p>'
        html+='<p>Comme sur cet exemple on suppose que 2 voies seulement ont été sauvegardées, <code>my_data</code> ne comporte que 4 colonnes (2 couples temps,tension).</p>'

        self.GuiMainLayout['pythonHelper'].setHtml(html)
        html = "<p><b>Fonctionnement du logiciel :</b></p>"
        html+= """<p>Le logiciel suppose que :<ul><li>Pour le GBF, aucune supposition.</li>Pour l'oscilloscope, que la voie 1 voit l'entrée du circuit, elle même connectée aussi au GBF, et que la voie 2 est connectée à la sortie. </li><li>Il est conseillé, si c'est possible, de placer l'oscilloscope en trigger externe en exploitant la voie 'sync' du générateur de fonctions comme signal de référence Trigger, pour éviter"""
        html+= "les problèmes de désynchronisation. Sinon utiliser la voie 1 comme référence de Trigger.</li></ul></p>"
        html+=  "<p><b>Fonctionnement interne : </b></p><p>Pour chaque fréquence :</p><ul>"
        html+= "<li>Le logiciel choisit une base de temps adaptée pour la fréquence voulue sur l'oscilloscope, et fait en sorte qu'il y ait entre 5 et 10 périodes à l'écran. Il paramètre aussi le générateur sur la même fréquence,</li>"
        html+= """<li>Après une première acquisition, regarde si la trace sort verticalement de la fenêtre d'acquisition, et si la trace n'est pas trop petite dans la fenêtre d'acquisition. Dans chacun de ces cas, le logiciel choisir un calibre mieux adapté, en divisant ou multipliant, selon le cas, la sensibilité verticale par un facteur 2.5 et refait une acquisition. Le logiciel répète cette étape jusqu'à 11 fois au maximum, et s'arrête lorsque la trace fait une taille acceptable à l'écran. Ce processus est répété simultanément pour les voies 1 et 2</li>"""
        html+= """<li>Une fois que les conditions d'acquisition sont bonnes, une fonction sinusoïdale à la fréquence délivrée par le générateur est ajustée en phase et amplitude sur les données acquises. L'algorithme utilisé est de type stochastique (<code>differential_evolution</code> de <code>scipy</code>), ce qui garantit la robustesse de cet ajustement. Après ajustement, les amplitudes extraites sont utilisées pour calculer module du diagramme de Bode, les phases sont utilisées pour calculer la phase du diagramme de Bode."""
        html+="""<li>La courbe d'erreur affichée est le résidu d'erreur entre les données et la courbe ajustée. Il est donc normal que l'erreur ne soit jamais nulle, à cause notamment du fait de la quantification verticale réalisée par l'oscilloscope lors de l'échantillonnage. Aussi, d'autres contributions à l'erreur sont possibles, notamment la présence de bruit de fond (mais l'ajustement doit minimiser l'impact sur les paramètres ajustés, c'est son but premier), ou l'erreur peut augmenter beaucoup si les signaux, notamment de sortie, sont déformés par rapport à une sinusoïde (par exemple effet du slew-rate en sortie d'un amplificateur opérationnel). C'est donc un paramètre à observer pour se poser la question de la qualité de la mesure. </li>"""
        html+=  "</ul>"

        self.GuiMainLayout['pythonHelper'].setReadOnly(True)


        self.GuiMainLayout['aboutMe'] = QtWidgets.QTextEdit()

        self.GuiMainLayout['aboutMe'].setHtml(html)
        self.GuiMainLayout['aboutMe'].setReadOnly(True)

        self.GuiMainLayout['coreTabs'] = QtWidgets.QTabWidget()
        self.GuiMainLayout['coreTabs'].addTab(self.GuiMainLayout['pythonHelper'], "Python et Matlab/Octave")
        self.GuiMainLayout['coreTabs'].addTab(self.GuiMainLayout['aboutMe'], "Fonctionnement de ce logiciel")
        self.GuiMainLayout['coreTabs'].addTab(self.GuiMainLayout['devListText'], "Liste des Appareils")

        self.GuiMainLayout['mainL'] = QtWidgets.QGridLayout()


        self.GuiMainLayout['mainL'].addWidget(self.GuiMainLayout['coreTabs'],0,0)
        self.setLayout(self.GuiMainLayout['mainL'])

    def closeEvent(self,event):
        super().closeEvent(event)
        self.aboutToClose.emit()


class QTitledComboBox(QtWidgets.QWidget):
    def __init__(self,*args,layout="vertical",labelTitle='',**kwargs):
        super().__init__(*args,**kwargs)
        if layout=="horizontal":
            self.layout=QtWidgets.QHBoxLayout()
        else: self.layout=QtWidgets.QVBoxLayout()
        self.title=QtWidgets.QLabel(labelTitle)
        font = self.title.font()
        font.setPointSize(10)
        font.setBold(True)
        self.title.setFont(font)
        self.combo=QtWidgets.QComboBox()

        self.layout.addWidget(self.title)
        self.layout.addWidget(self.combo)

        self.setLayout(self.layout)


class Window(QtWidgets.QMainWindow):

    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.buildGUILayout()
        self.penRed = {'color':'r'}#,'width': 2}
        self.penBlue = {'color':'b'}#,'width': 2}
        self.penMauve = {'color':(128, 0, 255)}#,'width': 2}
        self.infoWindow = None
        self.acquisitionMutex = QMutex()

    def buildGUILayout(self):
        self.setWindowTitle("Acquisition d'un diagramme de Bode")

        self.GuiMainLayout={}
        # choose scope part
        self.GuiMainLayout['scopesSupported'] = QtWidgets.QPushButton("Liste des Appareils\nSupportés")
        self.GuiMainLayout['scopesSupported'].clicked.connect(self.signal_scopesSupported)
        self.GuiMainLayout['pythonImport'] = QtWidgets.QPushButton("Exporter vers\nPython ou Matlab/Octave")
        self.GuiMainLayout['pythonImport'].clicked.connect(self.signal_pythonExample)
        self.GuiMainLayout['matlabImport'] = QtWidgets.QPushButton("Fonctionnement\nde ce logiciel")
        self.GuiMainLayout['matlabImport'].clicked.connect(self.signal_matlabExample)
        self.GuiMainLayout['devicesGroup'] = QtWidgets.QGroupBox("Choix des Appareils")
        self.GuiMainLayout['scopeCombo'] = QTitledComboBox(labelTitle='Oscilloscope')#QtWidgets.QComboBox()
        self.GuiMainLayout['scopeCombo'].combo.setMinimumWidth(100)
        self.GuiMainLayout['scopeCombo'].combo.setMaximumWidth(300)
        #for visaAddr,visaDev in app.functionGenerators.deviceList.items():
        #    print(visaAddr,visaDev)
        #addr = list(app.functionGenerators.deviceList.keys())[0]
        #app.functionGenerators.openCurrentDevice(addr)

        for visaAddr,visaDev in app.scopes.deviceList.items():
            name = visaDev[1]
            addr = visaAddr
            self.GuiMainLayout['scopeCombo'].combo.addItem(name+' [%s]'%addr)
        self.GuiMainLayout['scopeCombo'].combo.activated.connect(self.signal_scopeChanged)
        self.signal_scopeChanged(0)
        self.GuiMainLayout['funcGenCombo'] = QTitledComboBox(labelTitle='Générateur de Fonctions')#QtWidgets.QComboBox()
        self.GuiMainLayout['funcGenCombo'].combo.setMinimumWidth(100)
        self.GuiMainLayout['funcGenCombo'].combo.setMaximumWidth(300)
        for visaAddr,visaDev in app.functionGenerators.deviceList.items():
            name = visaDev[1]
            addr = visaAddr
            self.GuiMainLayout['funcGenCombo'].combo.addItem(name+' [%s]'%addr)
        self.GuiMainLayout['funcGenCombo'].combo.activated.connect(self.signal_functionGeneratorChanged)
        self.signal_functionGeneratorChanged(0)



        ## Build Layout for scope choice
        self.GuiMainLayout['devicesGroupLayout'] = QtWidgets.QHBoxLayout()
        self.GuiMainLayout['devicesGroupLayout'].addWidget(self.GuiMainLayout['scopeCombo'])
        self.GuiMainLayout['devicesGroupLayout'].addWidget(self.GuiMainLayout['funcGenCombo'])

        spacer = QtWidgets.QSpacerItem(10,10,QtWidgets.QSizePolicy.Expanding,QtWidgets.QSizePolicy.Minimum)
        self.GuiMainLayout['devicesGroupLayout'].addSpacerItem(spacer)
        self.GuiMainLayout['devicesGroupLayout'].addWidget(self.GuiMainLayout['pythonImport'])
        self.GuiMainLayout['devicesGroupLayout'].addWidget(self.GuiMainLayout['matlabImport'])
        self.GuiMainLayout['devicesGroupLayout'].addWidget(self.GuiMainLayout['scopesSupported'])
        self.GuiMainLayout['devicesGroup'].setLayout(self.GuiMainLayout['devicesGroupLayout'])

        # plots display part
        self.GuiMainLayout['displayGroup']=QtWidgets.QGroupBox("Diagramme de Bode Mesuré")
        pg.setConfigOption('antialias',True)
        pg.setConfigOption('background', 'w')
        pg.setConfigOption('foreground', 'k')
        #pg.setConfigOption('useOpenGL',True)
        pg.setConfigOption('leftButtonPan',False)
        self.GuiMainLayout['modulePlot'] = pg.PlotWidget()
        self.GuiMainLayout['phasePlot'] = pg.PlotWidget()
        self.GuiMainLayout['errorPlot'] = pg.PlotWidget()
        ## Build Layout for display part
        self.GuiMainLayout['displayGroupLayout']=QtWidgets.QVBoxLayout()
        self.GuiMainLayout['displayGroupLayout'].addWidget(self.GuiMainLayout['modulePlot'])
        self.GuiMainLayout['displayGroupLayout'].addWidget(self.GuiMainLayout['phasePlot'])
        self.GuiMainLayout['displayGroupLayout'].addWidget(self.GuiMainLayout['errorPlot'])
        self.GuiMainLayout['displayGroup'].setLayout(self.GuiMainLayout['displayGroupLayout'])

        # tools part
        self.GuiMainLayout['toolsGroup']=QtWidgets.QGroupBox("Pilotage et Paramétrage")
        self.GuiMainLayout['toolsGroup'].setMaximumWidth(250)
        self.GuiMainLayout['freqStartLabel'] = QtWidgets.QLabel('Fréquence de début (Hz) :')
        self.GuiMainLayout['freqStart'] = QtWidgets.QLineEdit('1')
        self.GuiMainLayout['freqStopLabel'] = QtWidgets.QLabel('Fréquence de fin (Hz) :')
        self.GuiMainLayout['freqStop'] = QtWidgets.QLineEdit('1e6')
        self.GuiMainLayout['freqModeLabel'] = QtWidgets.QLabel("Mode d'acquisition en fréquence :")
        self.GuiMainLayout['freqMode'] = QtWidgets.QComboBox()
        self.GuiMainLayout['freqPointsLabel'] = QtWidgets.QLabel('Nombre de points en fréquence:')
        self.GuiMainLayout['freqPoints'] = QtWidgets.QLineEdit('50')
        for mode in ('Répartition Linéaire','Répartition Multiplicative'):
            self.GuiMainLayout['freqMode'].addItem(mode)
        self.GuiMainLayout['freqMode'].setCurrentIndex(1)

        self.GuiMainLayout['inputVoltageLabel'] = QtWidgets.QLabel("Tension d'entrée (V) :")
        self.GuiMainLayout['inputVoltage']=QtWidgets.QLineEdit('100e-3')
        self.GuiMainLayout['moduleDisplayLabel'] = QtWidgets.QLabel('Afficher le module en :')
        self.GuiMainLayout['moduleDisplay'] = QtWidgets.QComboBox()
        for modBode in ('Gain en échelle Linéaire','Gain en échelle Logarithmique','Décibel'):
            self.GuiMainLayout['moduleDisplay'].addItem(modBode)
        self.GuiMainLayout['moduleDisplay'].setCurrentIndex(2)
        self.GuiMainLayout['moduleDisplay'].currentIndexChanged.connect(self.signal_moduleDisplayChanged)

        self.GuiMainLayout['phaseDisplayLabel'] = QtWidgets.QLabel('Afficher la phase en :')
        self.GuiMainLayout['phaseDisplay'] = QtWidgets.QComboBox()
        for phaseBode in ('Radians','Degrés'):
            self.GuiMainLayout['phaseDisplay'].addItem(phaseBode)
        self.GuiMainLayout['phaseDisplay'].setCurrentIndex(1)
        self.GuiMainLayout['phaseDisplay'].currentIndexChanged.connect(self.signal_phaseDisplayChanged)

        self.GuiMainLayout['frequencyDisplayLabel'] = QtWidgets.QLabel("Afficher l'axe des fréquences en : ")
        self.GuiMainLayout['frequencyDisplay'] = QtWidgets.QComboBox()
        for freqBode in ('Echelle Linéaire','Echelle Logarithmique'):
            self.GuiMainLayout['frequencyDisplay'].addItem(freqBode)
        self.GuiMainLayout['frequencyDisplay'].setCurrentIndex(1)
        self.GuiMainLayout['frequencyDisplay'].currentIndexChanged.connect(self.signal_frequencyDisplayChanged)


        self.GuiMainLayout['start'] = QtWidgets.QPushButton("Lancer\nla mesure")
        self.GuiMainLayout['start'].clicked.connect(self.signal_startBode)
        self.GuiMainLayout['abort'] = QtWidgets.QPushButton("Arrêter\nla mesure")
        self.GuiMainLayout['abort'].clicked.connect(self.signal_stopBode)
        self.GuiMainLayout['export'] = QtWidgets.QPushButton("Exporter\nau format CSV")
        self.GuiMainLayout['export'].clicked.connect(self.signal_exportCSV)
        for label in ('freqStartLabel','freqStopLabel','inputVoltageLabel',\
                      'freqModeLabel','freqPointsLabel','moduleDisplayLabel',\
                      'phaseDisplayLabel','frequencyDisplayLabel'):
            font = self.GuiMainLayout[label].font()
            font.setPointSize(10)
            font.setBold(True)
            self.GuiMainLayout[label].setFont(font)


        self.GuiMainLayout['quit'] = QtWidgets.QPushButton("Quitter")
        self.GuiMainLayout['quit'].clicked.connect(self.signal_quit)
        ## Build Layout for tools part
        self.GuiMainLayout['toolsGroupLayout']=QtWidgets.QVBoxLayout()
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['freqStartLabel'])
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['freqStart'])
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['freqStopLabel'])
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['freqStop'])
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['freqModeLabel'])
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['freqMode'])
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['freqPointsLabel'])
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['freqPoints'])
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['inputVoltageLabel'])
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['inputVoltage'])
        spacer = QtWidgets.QSpacerItem(10,10,QtWidgets.QSizePolicy.Minimum,QtWidgets.QSizePolicy.Expanding)
        self.GuiMainLayout['toolsGroupLayout'].addSpacerItem(spacer)
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['moduleDisplayLabel'])
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['moduleDisplay'])
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['phaseDisplayLabel'])
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['phaseDisplay'])
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['frequencyDisplayLabel'])
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['frequencyDisplay'])
        spacer = QtWidgets.QSpacerItem(10,10,QtWidgets.QSizePolicy.Minimum,QtWidgets.QSizePolicy.Expanding)
        self.GuiMainLayout['toolsGroupLayout'].addSpacerItem(spacer)
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['start'])
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['abort'])
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['export'])
        self.GuiMainLayout['toolsGroupLayout'].addWidget(self.GuiMainLayout['quit'])
        self.GuiMainLayout['toolsGroup'].setLayout(self.GuiMainLayout['toolsGroupLayout'])

        # Other elements
        self.GuiMainLayout['pbar'] = QtWidgets.QProgressBar()
        self.GuiMainLayout['pbar'].setGeometry(30, 40, 200, 25)
        self.GuiMainLayout['footer'] = QtWidgets.QLabel()
        self.GuiMainLayout['footer'].setTextFormat(Qt.RichText)
        from pyumeea.institution import base as eeahtml

        text = "<b>Acquisition d'un diagramme de Bode</b> v. 1.3 (May 2021)"
        text += '<br/><i>Ecrit en Python 3.7/pyQt5 par <A HREF="mailto:mikhael.myara@umontpellier.fr">mikhael.myara@umontpellier.fr</A></i><br/> <Université de Montpellier'
        text += '<br/><i>Département EEA - Faculté des Sciences</i><br/></i>Université de Montpellier</i>'
        text=eeahtml.eeaWidgetHTML_Qt(credits=text)
        self.GuiMainLayout['footer'].setText(text)

        # the main layout
        self.GuiMainLayout['mainL'] = QtWidgets.QGridLayout()
        self.GuiMainLayout['mainL'].addWidget(self.GuiMainLayout['devicesGroup'],0,0,1,3)
        self.GuiMainLayout['mainL'].addWidget(self.GuiMainLayout['displayGroup'],1,0)
        self.GuiMainLayout['mainL'].addWidget(self.GuiMainLayout['toolsGroup'],1,1)
        self.GuiMainLayout['mainL'].addWidget(self.GuiMainLayout['pbar'],2,0,1,3)
        self.GuiMainLayout['mainL'].addWidget(self.GuiMainLayout['footer'],3,0,1,3)
        self.GuiMainLayout['main'] = QtWidgets.QWidget()
        self.GuiMainLayout['main'].setLayout(self.GuiMainLayout['mainL'])
        # attribute the layout to this window
        self.setCentralWidget(self.GuiMainLayout['main'])

    #def closeEvent(self,event):
    def __del__(self):
        del app.scopes
        del app.functionGenerators
        # not clear why the closeEvent is not called ty QT ....
        # so I use the destructor.



    def signal_startBode(self):

        if self.acquisitionMutex.tryLock():

            freqStart = float(self.GuiMainLayout['freqStart'].text())
            freqStop = float(self.GuiMainLayout['freqStop'].text())
            freqPoints = int(self.GuiMainLayout['freqPoints'].text())

            frequencies = None
            if self.GuiMainLayout['freqMode'].itemText( self.GuiMainLayout['freqMode'].currentIndex() ) == 'Répartition Multiplicative':
                frequencies = np.logspace(np.log10(freqStart),np.log10(freqStop),freqPoints)
            else:
                frequencies = np.linspace(freqStart,freqStop,freqPoints)

            self.GuiMainLayout['pbar'].setRange(0,len(frequencies)-1)

            app.frequencies = frequencies
            app.bodeModule = np.empty(len(frequencies))
            app.bodePhase = np.empty(len(frequencies))
            app.bodeError = np.empty(len(frequencies))
            app.bodeModule[:]=np.nan
            app.bodePhase[:]=np.nan
            app.bodeError[:]=np.nan

            inputVoltage = float(self.GuiMainLayout['inputVoltage'].text())

            self.bodeWorker = BodeWorker()
            self.bodeWorker.importConditions(frequencies,inputVoltage)
            self.bodeThread = QThread()
            self.bodeWorker.moveToThread(self.bodeThread)
            self.bodeThread.started.connect(self.bodeWorker.run)
            self.bodeWorker.progress.connect(self.bodeProgress)
            self.bodeWorker.log.connect(self.bodeLog)

            ######### app.scopes.lockPanel(True)
            app.functionGenerators.lockPanel(True)

            self.bodeThread.start(QThread.HighestPriority)

    def signal_stopBode(self):
        self.bodeWorker.abort=True

    def bodeLog(self,string):
        if string=='####TERMINE####':
            self.doStopBodeThreadWorker()
        elif string.startswith('####ERROR####'):
            msgBox = QtWidgets.QMessageBox();
            msgBox.setText(string[13:]);
            msgBox.exec();
            self.doStopBodeThreadWorker()
        else:
            print(string)

    def doStopBodeThreadWorker(self):
        self.acquisitionMutex.unlock()
        self.bodeThread.quit()
        app.scopes.lockPanel(False)
        app.functionGenerators.lockPanel(False)


    def bodeProgress(self,inData):
        step = inData['step']
        self.GuiMainLayout['pbar'].setValue(step)
        app.bodeModule[step] = inData['module']
        app.bodePhase[step] = inData['phase']
        app.bodeError[step] = inData['error']
        self.refreshPlots()

    def refreshPlots(self):

        if len(app.frequencies) ==0 or len(app.bodeModule) ==0  or len(app.bodePhase) ==0  or len(app.bodeError) ==0:
            return

        moduleLog = (self.GuiMainLayout['moduleDisplay'].currentText() == "Gain en échelle Logarithmique")
        freqMode=(self.GuiMainLayout['frequencyDisplay'].currentText()=="Echelle Logarithmique")

        # module
        self.GuiMainLayout['modulePlot'].clear()
        if self.GuiMainLayout['moduleDisplay'].currentText() == "Décibel":
            localBodeModule = 20*np.log10(app.bodeModule)
        else: localBodeModule=app.bodeModule
        self.GuiMainLayout['modulePlot'].plot(x=app.frequencies,y=localBodeModule,pen=self.penRed)# ,pen=self.penCh1,name='Voie 1')
        self.GuiMainLayout['modulePlot'].setLogMode(freqMode,moduleLog)
        self.GuiMainLayout['modulePlot'].showGrid(x = True, y = True, alpha = 0.6)
        self.GuiMainLayout['modulePlot'].setLabel('left','Module')
        #self.GuiMainLayout['modulePlot'].setLabel('bottom','Frequence (Hz)')

        # phase
        self.GuiMainLayout['phasePlot'].clear()
        localBodePhase = app.bodePhase;#np.unwrap(app.bodePhase,discont=2*np.pi)
        if self.GuiMainLayout['phaseDisplay'].currentText()=="Degrés":
            localBodePhase = app.bodePhase*180/np.pi
        self.GuiMainLayout['phasePlot'].plot(x=app.frequencies,y=localBodePhase,pen=self.penRed)# ,pen=self.penCh1,name='Voie 1')
        self.GuiMainLayout['phasePlot'].setLogMode(freqMode,False)
        self.GuiMainLayout['phasePlot'].showGrid(x = True, y = True, alpha = 0.6)
        self.GuiMainLayout['phasePlot'].setLabel('left','Phase')
        #self.GuiMainLayout['phasePlot'].setLabel('bottom','Frequence (Hz)')
        # error
        self.GuiMainLayout['errorPlot'].clear()
        self.GuiMainLayout['errorPlot'].plot(x=app.frequencies,y=app.bodeError,pen=self.penMauve)# ,pen=self.penCh1,name='Voie 1')
        self.GuiMainLayout['errorPlot'].setLogMode(freqMode,False)
        self.GuiMainLayout['errorPlot'].showGrid(x = True, y = True, alpha = 0.6)
        self.GuiMainLayout['errorPlot'].setLabel('left','Erreur(%)')
        #self.GuiMainLayout['errorPlot'].setLabel('bottom','Frequence (Hz)')

    def signal_moduleDisplayChanged(self):
        self.refreshPlots()

    def signal_phaseDisplayChanged(self):
        self.refreshPlots()

    def signal_frequencyDisplayChanged(self):
        self.refreshPlots()

    def signal_exportCSV(self):

        if len(app.frequencies) ==0 or len(app.bodeModule) ==0  or len(app.bodePhase) ==0  or len(app.bodeError) ==0:
            return
        if self.acquisitionMutex.tryLock():

            options = QtWidgets.QFileDialog.Options()
            #options |= QFileDialog.DontUseNativeDialog
            fileName, _ = QtWidgets.QFileDialog.getSaveFileName(self,"QFileDialog.getSaveFileName()","","CSV Files (*.csv);;TSV Files (*.tsv);;Text Files (*.txt);;All Files (*)", options=options)

            if fileName != '':
                dataToStore=[]
                headers=['frequence(Hz),Module,phase(rd),erreur(%)']
                dataToStore.append(app.frequencies)
                dataToStore.append(app.bodeModule)
                dataToStore.append(app.bodePhase)
                dataToStore.append(app.bodeError)

                toStore = np.column_stack(dataToStore)
                header = ','.join(headers)

                np.savetxt(fileName, toStore,  delimiter=',',header=header)
            self.acquisitionMutex.unlock()

    def signal_scopeChanged(self,forceIdx = None):
        if forceIdx == None:
            idx  =  self.GuiMainLayout['scopeCombo'].currentIndex()
        else:
            idx = forceIdx
        if(idx >= 0):
            scopeAddr = list(app.scopes.deviceList.keys())[idx]
            app.scopes.openCurrentDevice(scopeAddr)

    def signal_functionGeneratorChanged(self,forceIdx=None):
        if forceIdx == None:
            idx  =  self.GuiMainLayout['scopeCombo'].currentIndex()
        else:
            idx = forceIdx
        if(idx >= 0):
            funcGenAddr = list(app.functionGenerators.deviceList.keys())[idx]
            app.functionGenerators.openCurrentDevice(funcGenAddr)

    def signal_quit(self):
        del app.scopes
        del app.functionGenerators
        app.quit()


    def createInfoWindow(self):
        if( self.infoWindow == None):
            theWindow = infoWindow()
            theWindow.setAttribute(Qt.WA_DeleteOnClose);
            theWindow.aboutToClose.connect(self.signal_infoWindowClosed)
            self.infoWindow = theWindow

    def signal_scopesSupported(self):
        self.createInfoWindow()
        self.infoWindow.GuiMainLayout['coreTabs'].setCurrentIndex(2)
        self.infoWindow.show()


    def signal_pythonExample(self):
        self.createInfoWindow()
        self.infoWindow.GuiMainLayout['coreTabs'].setCurrentIndex(0)
        self.infoWindow.show()


    def signal_matlabExample(self):
        self.createInfoWindow()
        self.infoWindow.GuiMainLayout['coreTabs'].setCurrentIndex(1)
        self.infoWindow.show()


    def closeEvent(self,event):
        if( self.infoWindow != None):
            self.infoWindow.close()
        super().closeEvent(event)

    def signal_infoWindowClosed(self):
        self.infoWindow = None

app = QtWidgets.QApplication(sys.argv)
# try to find a visa driver. If not switch to pyvisa-pi
visaDriver = ''
try:
    rm = visa.ResourceManager(visaDriver)
    rm.close()
    del rm
except:
    visaDriver='@py'


try:
    # instenciate instruments drivers and start connection
    app.scopes = scopesHandler(visaDriver)
    app.functionGenerators = functionGeneratorsHandler(visaDriver)
    # build basic empty data
    app.frequencies = np.zeros(0)
    app.bodeModule = np.zeros(0)
    app.bodePhase = np.zeros(0)
    app.bodeError = np.zeros(0)

    # GUI management
    win = Window()
    win.show()
    sys.exit(app.exec())

    # error before GUI starts management
except ValueError as e:
    msgBox = QtWidgets.QMessageBox();
    msgBox.setText(str(e));
    msgBox.exec()

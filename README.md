# instrumentation

Contains instrumentation software (not instrument drivers, please see pyumeea project for this) for EEA department, like scope trace acquisition, bode diagram acquisition, etc. See below for installation.

## Contents

### ScopeEEA
Allows to acquire traces for supported oscilloscopes. Exports results as CSV file.
![ScopeEEA](_Pics_/ScopeEEA.png)


### BodeEEA
Allows to acquire bode graph from a scope and a function generator.
Both have to be known by the software. At this time  it is made for pymeasure-like devices, and works with USB connected DSO or DSOX scopes by Agilent/Keysight and 332x0A function generators by Agilent/Keysight.

The software auto chooses the good timebase and voltage calibration for eacth frequency. It uses stochastic algorithm (`differential_evolution` from `scipy`) to fit sine-wave parameters from acquired data for input and output signals. This fit allows to drastically reduce the influence of noise (digital as well as analog noise) and makes the results far more accurante. From fitted parameters, it computes magnitude and phase for bode plot. It displays bode plot and fit error for each measured point.

Works on Mac and Linux, chooses installed Visa driver or uses pyvisa-pi driver if required. No reason so that it could not work on Windows (to be checked however).

Depending on the system configuration, the visa devices could be not recognized because rights are not granted. It should not happen with the official NI-Visa Driver but may happen with pyvisa-pi. You can check this by running the software in administrator mode. With Linux, you can easily create rules with udev on most linux distributions for that.

It requires the pyumeea package (on same gitlab repository) to work. The package should be copied at the same level as the BodeEEA.py or in the python path with the other libraries.

Exports results as CSV file.

![BodeEEA](_Pics_/BodeEEA.png)


## installation
Both BodeEEA and ScopeEEA were developped for Debian 10 // Linux Mint XFCE 20 with Anaconda 4.9.2 but should work in other contexts.
### os dependencies
Under Mint, some extra packages are required. Simply from terminal :
```bash
apt install git
```
### python dependencies
Then use anaconda's python instead of the system python (should work too but some pip installation should be necessary, not tested). If installed for all users, it should be in /opt. If true, all the following operations should be made in superuer mode. Then, All that is needed is to activate anaconda context. If you use various anaconda contexts, please activate the good one. Il most cases, the following may be ok  :
```bash
sudo su
/opt/anaconda3/bin/conda init
bash
```
To check it is the good python that works, simply run `python3` from command line. You should see it is anaconda's python that works.

Next we need to install extra conda packages :
```bash
conda install pyqtgraph
conda install -c conda-forge -y pyvisa
conda install -c conda-forge -y pymeasure
```
If you have an officiel National instrument visa driver installed it should be enough. However, surprizingly, the Debian and Debian-dependent distributions are not supported at this time (July 2021) whereas other, less used as desktop stations (by far !) are supported. Fortunately, a python visa driver has been developped, making USB VISA devices work even without official VISA driver (and it works very well). To add it, please run on terminal :
```bash
conda install -c conda-forge -y pyvisa-py
```
### other dependencies : add home made "pyumeea" library
Add this time there is no conda or pip-thing for required pyumeea library and so we have to copy it by ourselves. We first have to get it from git :
```bash
cd 
git clone https://gitlab.com/umeea/pyumeea
cd pyumeea
```

Then we have to find the path where python store liraries. To check that, please run in terminal :
```
python3 -c "import sys; print('\n'.join(sys.path))"
```
In my case, I get :
```
/opt/anaconda3/lib/python38.zip
/opt/anaconda3/lib/python3.8
/opt/anaconda3/lib/python3.8/lib-dynload
/opt/anaconda3/lib/python3.8/site-packages
/opt/anaconda3/lib/python3.8/site-packages/locket-0.2.1-py3.8.egg
```

And so I copy `pyumeea` in one of these :
```
cp -R ./pyumeea /opt/anaconda3/lib/python3.8/site-packages
```



### Set usb devices rights
In Debian or Mint, we should set the udev for that, in root mode.
You the USB identifiers of the devices. Start the device and connect it with a usb cable to the computer. Then :
```bash
sudo su
lsusb
```
On my computer, with two agilent devices connected, I get :
```
Bus 001 Device 003: ID  Agilent Technologies, Inc. DSO-X 2002A
Bus 001 Device 002: ID 0957:0407 Agilent Technologies, Inc. 33220A Waveform Generator
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 005 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hub
Bus 004 Device 003: ID 413c:2106 Dell Computer Corp. Dell QuietKey Keyboard
Bus 004 Device 002: ID 046d:c05a Logitech, Inc. M90/M100 Optical Mouse
Bus 004 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hub
Bus 003 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hub
Bus 002 Device 001: ID 1d6b:0001 Linux Foundation 1.1 root hub
```
You identify the two Agilent devices, a DSO-X 2002A and a 3320A WGen. Please note the USB IDs. For example, for the DSO-X, it is written to be 0957:179b.
We then have to build a udev file that contains this information to allow access to this device for any kind of user. To do this :
```
pico /etc/udev/rules.d/30-myScopes.rules
```
This file may not exist if you did not already create it. Paste inside (do not replace : add this to the file if already exists) the following sequence but customized for the usb device you need :
```
SUBSYSTEMS=="usb", ATTRS{idVendor}=="0957", ATTRS{idProduct}=="179b", \
MODE:="0666", \
SYMLINK+="agilent_DSO2002_%n"
```
You can identify the USB Ids in idVendor and idProduct. Sample files are given in this git.

Then, you can restart udev configuration, or restart the computer, both should work.
```bash
udevadm control --reload-rules && udevadm trigger
```


### Install the softwares themselves
We just have to copy the softwares themselve. We guess a good location should be `/opt/umeea/bin` but any other location may be ok. For example for BodeEEA, we should do :
```bash
cd
cd instrumentation
mkdir -p /opt/umeea/bin
cp ./BodeEEA/bin/* /opt/umeea/bin/
```
You perhaps should want to update the paths inside the small .sh files to fit your configuration.

### Install "desktop" and icon
Desktop and icon are available too, both in the Desktop folder of each application. Installation should be easy. If we are in the BodeEEA folder for example :
cp ./Desktop/*.desktop /usr/share/applications/
cp ./Desktop/*.png /usr/share/icons/

and that's all. Again, the desktop file should be edited to adjust paths, depending on your configuation.
